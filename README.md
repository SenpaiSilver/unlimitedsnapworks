# Unlimited Snapworks

Screenshoting app that will screenshot in:
* PNG
* BMP
* JPEG
* TIFF

# Future features

* FPS: Will capture a certain amount of frames per second each time the hotkey is pressed;
* Autoupdater: The app will autoupdate on it's own everytime you launch it;
* Settings: It will be possible to save your settings;
* Launch at boot: You won't need to launch the app everytime you boot into Windows;
* System tray info: Graphical feedback will be given in the system tray.