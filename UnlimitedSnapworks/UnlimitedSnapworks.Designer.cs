﻿namespace UnlimitedSnapworks
{
    partial class UnlimitedSnapworks
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.labelBrowse = new System.Windows.Forms.Label();
			this.textBoxBrowse = new System.Windows.Forms.TextBox();
			this.buttonBrowse = new System.Windows.Forms.Button();
			this.labelKey = new System.Windows.Forms.Label();
			this.textBoxKey = new System.Windows.Forms.TextBox();
			this.labelFileformat = new System.Windows.Forms.Label();
			this.textBoxOutput = new System.Windows.Forms.TextBox();
			this.panelText = new System.Windows.Forms.Panel();
			this.buttonAbout = new System.Windows.Forms.Button();
			this.comboBoxFormat = new System.Windows.Forms.ComboBox();
			this.statusStrip = new System.Windows.Forms.StatusStrip();
			this.toolStripStatusLabelFocus = new System.Windows.Forms.ToolStripStatusLabel();
			this.buttonSnap = new System.Windows.Forms.Button();
			this.Timer = new System.Windows.Forms.Timer(this.components);
			this.panelText.SuspendLayout();
			this.statusStrip.SuspendLayout();
			this.SuspendLayout();
			// 
			// labelBrowse
			// 
			this.labelBrowse.AutoSize = true;
			this.labelBrowse.Location = new System.Drawing.Point(0, 9);
			this.labelBrowse.Name = "labelBrowse";
			this.labelBrowse.Size = new System.Drawing.Size(93, 13);
			this.labelBrowse.TabIndex = 0;
			this.labelBrowse.Text = "Screenshot folder:";
			// 
			// textBoxBrowse
			// 
			this.textBoxBrowse.Location = new System.Drawing.Point(99, 6);
			this.textBoxBrowse.Name = "textBoxBrowse";
			this.textBoxBrowse.ReadOnly = true;
			this.textBoxBrowse.Size = new System.Drawing.Size(279, 20);
			this.textBoxBrowse.TabIndex = 1;
			// 
			// buttonBrowse
			// 
			this.buttonBrowse.Location = new System.Drawing.Point(384, 4);
			this.buttonBrowse.Name = "buttonBrowse";
			this.buttonBrowse.Size = new System.Drawing.Size(75, 23);
			this.buttonBrowse.TabIndex = 2;
			this.buttonBrowse.Text = "Browse";
			this.buttonBrowse.UseVisualStyleBackColor = true;
			this.buttonBrowse.Click += new System.EventHandler(this.Browse);
			// 
			// labelKey
			// 
			this.labelKey.AutoSize = true;
			this.labelKey.Location = new System.Drawing.Point(0, 35);
			this.labelKey.Name = "labelKey";
			this.labelKey.Size = new System.Drawing.Size(85, 13);
			this.labelKey.TabIndex = 3;
			this.labelKey.Text = "Screenshot Key:";
			// 
			// textBoxKey
			// 
			this.textBoxKey.Location = new System.Drawing.Point(99, 32);
			this.textBoxKey.Name = "textBoxKey";
			this.textBoxKey.ReadOnly = true;
			this.textBoxKey.Size = new System.Drawing.Size(100, 20);
			this.textBoxKey.TabIndex = 4;
			this.textBoxKey.Text = "A";
			this.textBoxKey.Click += new System.EventHandler(this._PromptBind);
			this.textBoxKey.KeyDown += new System.Windows.Forms.KeyEventHandler(this._SetBind);
			this.textBoxKey.Leave += new System.EventHandler(this._LeaveBind);
			// 
			// labelFileformat
			// 
			this.labelFileformat.AutoSize = true;
			this.labelFileformat.Location = new System.Drawing.Point(0, 61);
			this.labelFileformat.Name = "labelFileformat";
			this.labelFileformat.Size = new System.Drawing.Size(58, 13);
			this.labelFileformat.TabIndex = 5;
			this.labelFileformat.Text = "File format:";
			// 
			// textBoxOutput
			// 
			this.textBoxOutput.Location = new System.Drawing.Point(3, 3);
			this.textBoxOutput.Multiline = true;
			this.textBoxOutput.Name = "textBoxOutput";
			this.textBoxOutput.ReadOnly = true;
			this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBoxOutput.Size = new System.Drawing.Size(456, 163);
			this.textBoxOutput.TabIndex = 8;
			this.textBoxOutput.TextChanged += new System.EventHandler(this.ScrollToEnd);
			// 
			// panelText
			// 
			this.panelText.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.panelText.Controls.Add(this.textBoxOutput);
			this.panelText.Location = new System.Drawing.Point(0, 126);
			this.panelText.Name = "panelText";
			this.panelText.Size = new System.Drawing.Size(462, 191);
			this.panelText.TabIndex = 9;
			// 
			// buttonAbout
			// 
			this.buttonAbout.Location = new System.Drawing.Point(384, 30);
			this.buttonAbout.Name = "buttonAbout";
			this.buttonAbout.Size = new System.Drawing.Size(75, 23);
			this.buttonAbout.TabIndex = 10;
			this.buttonAbout.Text = "About";
			this.buttonAbout.UseVisualStyleBackColor = true;
			this.buttonAbout.Click += new System.EventHandler(this.About);
			// 
			// comboBoxFormat
			// 
			this.comboBoxFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxFormat.FormattingEnabled = true;
			this.comboBoxFormat.Items.AddRange(new object[] {
            "PNG",
            "JPEG",
            "BMP",
            "TIFF"});
			this.comboBoxFormat.Location = new System.Drawing.Point(99, 58);
			this.comboBoxFormat.Name = "comboBoxFormat";
			this.comboBoxFormat.Size = new System.Drawing.Size(100, 21);
			this.comboBoxFormat.TabIndex = 11;
			// 
			// statusStrip
			// 
			this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelFocus});
			this.statusStrip.Location = new System.Drawing.Point(0, 295);
			this.statusStrip.Name = "statusStrip";
			this.statusStrip.Size = new System.Drawing.Size(462, 22);
			this.statusStrip.SizingGrip = false;
			this.statusStrip.TabIndex = 12;
			this.statusStrip.Text = "statusStrip";
			// 
			// toolStripStatusLabelFocus
			// 
			this.toolStripStatusLabelFocus.Name = "toolStripStatusLabelFocus";
			this.toolStripStatusLabelFocus.Size = new System.Drawing.Size(107, 17);
			this.toolStripStatusLabelFocus.Text = "No focus detected.";
			// 
			// buttonSnap
			// 
			this.buttonSnap.Location = new System.Drawing.Point(384, 56);
			this.buttonSnap.Name = "buttonSnap";
			this.buttonSnap.Size = new System.Drawing.Size(75, 23);
			this.buttonSnap.TabIndex = 13;
			this.buttonSnap.Text = "Snap !";
			this.buttonSnap.UseVisualStyleBackColor = true;
			this.buttonSnap.Click += new System.EventHandler(this.Snap);
			// 
			// Timer
			// 
			this.Timer.Enabled = true;
			this.Timer.Tick += new System.EventHandler(this.TimerTick);
			// 
			// UnlimitedSnapworks
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(462, 317);
			this.Controls.Add(this.buttonSnap);
			this.Controls.Add(this.statusStrip);
			this.Controls.Add(this.comboBoxFormat);
			this.Controls.Add(this.buttonAbout);
			this.Controls.Add(this.panelText);
			this.Controls.Add(this.labelFileformat);
			this.Controls.Add(this.textBoxKey);
			this.Controls.Add(this.labelKey);
			this.Controls.Add(this.buttonBrowse);
			this.Controls.Add(this.textBoxBrowse);
			this.Controls.Add(this.labelBrowse);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.HelpButton = true;
			this.MaximizeBox = false;
			this.Name = "UnlimitedSnapworks";
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.Text = "Unlimited Snapworks";
			this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this._MainWindowClosed);
			this.Load += new System.EventHandler(this._MainWindowLoad);
			this.panelText.ResumeLayout(false);
			this.panelText.PerformLayout();
			this.statusStrip.ResumeLayout(false);
			this.statusStrip.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBrowse;
        private System.Windows.Forms.TextBox textBoxBrowse;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.Label labelKey;
        private System.Windows.Forms.TextBox textBoxKey;
        private System.Windows.Forms.Label labelFileformat;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Panel panelText;
        private System.Windows.Forms.Button buttonAbout;
        private System.Windows.Forms.ComboBox comboBoxFormat;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelFocus;
        private System.Windows.Forms.Button buttonSnap;
		private System.Windows.Forms.Timer Timer;
    }
}

