﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;

namespace UnlimitedSnapworks
{
    public partial class UnlimitedSnapworks : Form
    {
        private int Key;
        private FolderBrowserDialog BrowseDialog;
        private Screenshot.GetCurrentWindowHandle CurrentWindowHandle;
        private IntPtr hand;

        public UnlimitedSnapworks()
        {
            InitializeComponent();
			this.Key = 0x00;
            this.BrowseDialog = new FolderBrowserDialog();
            this.CurrentWindowHandle = new Screenshot.GetCurrentWindowHandle();
            this.textBoxBrowse.Text = Directory.GetCurrentDirectory() + @"\snap\";
            if (!Directory.Exists(this.textBoxBrowse.Text))
                Directory.CreateDirectory(this.textBoxBrowse.Text);
        }

        private ImageFormat Format()
        {
            if (this.comboBoxFormat.SelectedItem.ToString() == "PNG")
                return (ImageFormat.Png);
            else if (this.comboBoxFormat.SelectedItem.ToString() == "JPEG" || this.comboBoxFormat.SelectedItem.ToString() == "JPG")
                return (ImageFormat.Jpeg);
            else if (this.comboBoxFormat.SelectedItem.ToString() == "TIFF")
                return (ImageFormat.Tiff);
            else if (this.comboBoxFormat.SelectedItem.ToString() == "BMP")
                return (ImageFormat.Bmp);
            else
                return (ImageFormat.Png);
        }

        public void SaveScreenshot(string file, DateTime time)
        {
            Rectangle Bounds;
            Graphics Graph;
            Bitmap bmp;

            file = this.textBoxBrowse.Text + file + "." + this.comboBoxFormat.SelectedItem.ToString().ToLower();
            Bounds = this.Bounds;
            bmp = new Bitmap(Bounds.Width, Bounds.Height);
            Graph = Graphics.FromImage(bmp);
            Graph.CopyFromScreen(new Point(Bounds.Left, Bounds.Top), Point.Empty, Bounds.Size);
            bmp.Save(file, Format());
            this.textBoxOutput.Text += "[" + time.TimeOfDay + "] " + file;
            if (!File.Exists(this.textBoxBrowse.Text + file))
                this.textBoxOutput.Text += " Saved.\r\n";
            else
                this.textBoxOutput.Text += " Not saved.\r\n";
        }

        public void SaveScreenshotWindow(string file, DateTime time)
        {
            Screenshot.ScreenCapture sc;
            sc = new Screenshot.ScreenCapture();
            file = this.textBoxBrowse.Text + file + "." + this.comboBoxFormat.SelectedItem.ToString().ToLower();
            sc.CaptureWindowToFile(this.hand, file, this.Format());
            this.textBoxOutput.Text += "[" + time.TimeOfDay + "] " + file;
            if (!File.Exists(this.textBoxBrowse.Text + file))
                this.textBoxOutput.Text += " Saved.\r\n";
            else
                this.textBoxOutput.Text += " Not saved.\r\n";
        }

        private void Snap(object sender, EventArgs e)
        {
            string file;
            int i;
            DateTime time;

            //Thread.Sleep((int)this.numericTimer.Value * 1000);
            this.UpdateHandle();
            this.hand = (IntPtr) int.Parse(this.CurrentWindowHandle.Handle);
            time = DateTime.Now;
            file = this.CurrentWindowHandle.Buff.Replace(' ', '_');
            file += "_" + time.Year + "-" + time.Month + "-" + time.Day + "-" + time.Hour + "-" + time.Minute + "-" + time.Second + "-";
            i = -1;
            while (File.Exists(this.textBoxBrowse.Text + "\\" + file + (++i).ToString() + "." + this.comboBoxFormat.SelectedItem.ToString().ToLower()))
                ;
            this.SaveScreenshotWindow(file + i.ToString(), time);
        }

        private void UpdateHandle()
        {
            this.CurrentWindowHandle.GetActiveWindow();
            this.toolStripStatusLabelFocus.Text = this.CurrentWindowHandle.Buff + " " + this.CurrentWindowHandle.Handle + " had focus.";
            //this.textBoxOutput.Text += this.toolStripStatusLabelFocus.Text + "\r\n";
        }

        private void Browse(object sender, EventArgs e)
        {
            this.BrowseDialog.RootFolder = Environment.SpecialFolder.Desktop;
            this.BrowseDialog.ShowNewFolderButton = false;
            this.BrowseDialog.ShowDialog();
            if (this.BrowseDialog.SelectedPath != "")
                this.textBoxBrowse.Text = this.BrowseDialog.SelectedPath;
        }

        private void About(object sender, EventArgs e)
        {
        }

        private void TimerTick(object sender, EventArgs e)
        {
            UpdateHandle();
        }

        private void ScrollToEnd(object sender, EventArgs e)
        {
            this.textBoxOutput.SelectionStart = this.textBoxOutput.Text.Length;
            this.textBoxOutput.ScrollToCaret();
            this.textBoxOutput.Refresh();
		}

		protected override void WndProc(ref Message m)
		{
			if (m.Msg == 0x0312)
				this.Snap(null, null);
			base.WndProc(ref m);
		}

		private void _MainWindowLoad(object sender, EventArgs e)
		{
			this.comboBoxFormat.SelectedIndex = 0;
			this.NewHotkey('A');
		}

		private void _MainWindowClosed(object sender, FormClosedEventArgs e)
		{
			if (this.Key != 0x00)
				UnregisterHotKey(this.Handle, this.Key);
		}

		private bool NewHotkey(int key)
		{
			if (this.Key != 0x00)
				UnregisterHotKey(this.Handle, this.Key);
			return (RegisterHotKey(this.Handle, 1, 0x4000, key));
		}

		[DllImport("user32.dll")]
		public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fdModifiers, int vk);
		[DllImport("user32.dll")]
		public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

		private void _PromptBind(object sender, EventArgs e)
		{
			this.textBoxKey.Text = "Bind a key...";
		}

		private void _SetBind(object sender, KeyEventArgs e)
		{
			this.textBoxKey.Text = Convert.ToChar(e.KeyValue).ToString();
			this.Key = e.KeyValue;
		}

		private void _LeaveBind(object sender, EventArgs e)
		{
			this.textBoxKey.Text = Convert.ToChar(this.Key).ToString();
		}
    }
}
